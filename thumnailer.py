import os, json, sys, re
from PIL import Image
if __name__ == "__main__":
    print("thumbnailer.py imagesdir thumb_size html,jdict,json")
    print(sys.argv)
    image_dir = sys.argv[1]
    thumbnails_dir = os.path.join(image_dir,"thumbnails");
    if not os.path.exists(thumbnails_dir):
        os.makedirs(thumbnails_dir)
    size = int(sys.argv[2]),int(sys.argv[2])
    #to select onliy images
    r = re.compile(r"\.(jpg|JPG|jpeg|JPEG|PNG|png)$")

    image_data=[]
    for x in os.listdir(image_dir):
        if not r.search(x):
            continue;
        d={}
        y = os.path.join(image_dir,x)
        im = Image.open(y)
        w,h = im.size
        im.thumbnail(size)
        z = os.path.join(thumbnails_dir,x)
        im.save(z)
        d['image']=y
        d['thumbnail']=z
        d['w']=w
        d['h']=h
        image_data.append(d)

    if len(sys.argv)>3:
        extra = sys.argv[3]
        extra_dir = os.path.join(image_dir,"extra_data");
        if not os.path.exists(extra_dir):
            os.makedirs(extra_dir)
        if "html" in extra:
            print("Writing html code for agallery")
            with open(os.path.join(extra_dir,"agallery.html"), "w") as fp:
                print('<div class="agallery">',file=fp)
                for adata in image_data:
                    image = adata['image']
                    thumbnail = adata['thumbnail']
                    w = adata['w']
                    h = adata['h']
                    strin = f'<div class="aphoto">\n<img abigphotolink="{image}" asize="{w}x{h}" src="{thumbnail}">\n</div>'
                    print(strin, file=fp)
                print('</div>',file=fp)

        if "jdict" in extra:
            print("Writing jdict code for agallery")
            with open(os.path.join(extra_dir,"images_data_js_dict.json"), "w") as fp:
                jdata = json.dumps(image_data)
                print("images_data='"+jdata+"';",file=fp)

        if "json" in extra:
            print("Writing json code for agallery")
            with open(os.path.join(extra_dir,"images_data.json"), "w") as fp:
                jdata = json.dumps(image_data)
                print(jdata,file=fp)

    print("Operation completed successfully")
    print("Data files and thumbnails generated in "+image_dir);
